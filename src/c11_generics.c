#include <stdio.h>

#define type_idx(T) _Generic( (T), char: 1, int: 2, long: 3, struct a_struct: 4, default: 0)
#define type_idx_no_default(T) _Generic( (T), char: 1, int: 2, long: 3, struct a_struct: 4)

struct a_struct {
    int a;
    int b;
};

struct b_struct {
    int c;
    int d;
};

int main(void)
{
    printf("%d\n", type_idx('a'));
    struct a_struct a = { .a = 1, .b = 2};
    printf("%d\n", type_idx(a));
    printf("%d\n", type_idx_no_default(a));

    struct b_struct b = { .c = 1, .d = 2};
    printf("%d\n", type_idx(b));
    // printf("%d\n", type_idx_no_default(b));
}
