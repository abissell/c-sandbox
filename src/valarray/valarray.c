/// valarray.c
/// Testing performance differences of memcpy vs statically known
/// array accesses and stores

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "valarray.h"

char rand_c_a[10000];
int rand_i_a[10000];
double rand_f_a[10000];

int rand_i_b[10000];
double rand_f_b[10000];

struct mem_container *mem_container_create(size_t elem_size, unsigned length)
{
    struct mem_container *mem_cont = (struct mem_container *)malloc(elem_size * length);
    return mem_cont;
}

void mem_container_set(struct mem_container *container, void *data, unsigned idx)
{
    char *p = (char *)container->data;
    p += idx * container->elem_size;
    memcpy(p, data, container->elem_size);
}

void fill_arrays(void)
{
    for (int i = 0; i < 10000; ++i) {
        rand_c_a[i] = (char) rand();
        rand_i_a[i] = rand();
        rand_f_a[i] = (float) rand();

        rand_i_b[i] = rand();
        rand_f_b[i] = (float) rand();
    }
}

int main(void)
{
    fill_arrays();
    printf("%d\n", rand_i_a[9875]);
}
