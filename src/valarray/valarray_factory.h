// http://pic.dhe.ibm.com/infocenter/zos/v2r1/index.jsp?topic=%2Fcom.ibm.zos.v2r1.cbclx01%2Fgenericselection.htm

struct a_strct {
    char c;
    int i;
    float f;
};

struct b_strct {
    int i;
    float f;
};

#define container(X) \
struct cont { \
    X *data; \
    unsigned length; \
}; \

// Define the generic container for different value types
#define gen_container(X) _Generic((X), \
struct a_strct: a_strct_container, \
struct b_strct: b_strct_container)
