#include "valarray_factory.h"

gen_container(struct a_strct) a_str_con;

struct mem_container {
    void *data;
    size_t elem_size;
    unsigned length;
};

struct mem_container *mem_container_create(size_t elem_size, unsigned length);

void mem_container_set(struct mem_container *container, void *data, unsigned idx);

struct gen_container {
    void *data;
    unsigned length;
};

// struct gen_container *gen_container_create(size_t elem_size, unsigned length);

// void gen_container_set(struct gen_container *container, void *val);
