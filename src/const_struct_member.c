#include <stdio.h>
#include <stdlib.h>

struct a_struct {
    const member;
};

struct a_struct init_struct_1(void)
{
    struct a_struct init = { .member = 1 };
    return init;
}

struct a_struct *init_struct_2(void)
{
    struct a_struct *init = (struct a_struct *)malloc(sizeof(struct a_struct));
    int *const member = &init->member;
    *member = 2;
    return init;
}

int main(void)
{
    struct a_struct init_1 = init_struct_1();
    printf("%d\n", init_1.member);
    struct a_struct *init_2 = init_struct_2();
    printf("%d\n", init_2->member);
}
