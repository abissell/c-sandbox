#include <stdio.h>
#include <limits.h>

enum Predefined {
    FIRST,
    SECOND
};

int main(void)
{
    enum Predefined predef = FIRST;
    printf("%d\n", predef);
    predef = SECOND;
    printf("%u\n", predef);
}
