#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct test {
    int a;
    float b;
};

int main(void)
{
    struct test vals = { .a = 1, .b = 3.45 };

    const size_t test_size = sizeof(struct test);

    const unsigned capacity = 5;
    char *data = (char *)malloc(capacity * sizeof(struct test));

    char *vals_ptr = (char *) &vals;

    for (unsigned i = 0; i < test_size; ++i) {
        data[i] = vals_ptr[i];
    }

    memcpy(&data[test_size], vals_ptr, test_size);

    printf("%d %f\n", vals.a, vals.b);

    struct test *test_data = (struct test *)data;
    printf("%d %f\n", test_data->a, test_data->b);

    printf("%d %f\n", test_data[1].a, test_data[1].b);
}
