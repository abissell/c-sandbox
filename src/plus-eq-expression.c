#include <stdio.h>

int main()
{
    int val = 5;
    printf("result of expression 5 += 3: %d\n", val += 3);
    printf("after evaluation: %d\n", val);
}
