#include <stdio.h>
#include <limits.h>

static inline unsigned advance_idx(unsigned idx, const unsigned adv,
                                   const unsigned capacity)
{
    return (idx + adv) & (capacity - 1);
}

static inline unsigned decrement_idx(unsigned idx, const unsigned capacity)
{
    return (idx - 1) & (capacity - 1);
}

int main(void)
{
    unsigned capacity = 64;
    unsigned i = 63;
    printf("i: %u\n", i);
    i = advance_idx(i, 1, capacity);
    printf("adv by 1: %u\n", i);
    i = advance_idx(i, 1, capacity);
    printf("adv by 1: %u\n", i);
    i = 0;
    printf("reset to 0\n");
    i = advance_idx(i, 0, capacity);
    printf("adv by 0: %u\n", i);

    i = UINT_MAX / 2;
    printf("UINT_MAX / 2: %u\n", i);
    i = i * 2;
    printf("prev * 2: %u\n", i);

    i = 0;
    printf("reset i to %u\n", i);
    i = decrement_idx(i, capacity);
    printf("decremented i = %u\n", i);
}
