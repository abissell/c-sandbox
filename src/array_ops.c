#include <stdbool.h>
#include <stdio.h>

struct a_struct {
    bool is_active;
    int a;
};

struct b_struct {
    int b;
    double c;
    void (*describe)(void *self);
};

bool a_str_is_active(void *self)
{
    struct a_struct *a_str = self;
    if (a_str->is_active) {
        printf("a_struct: %d\n", a_str->a);
    }
}

struct b_struct b_array[200];

void generic_func(void *array, bool (*is_active)(void *), size_t elem_size, size_t array_size)
{
    for (int i = 0; i < array_size; ++i) {
        is_active(array + i*elem_size);
    }
}

int main()
{
    struct a_struct a_array[100];
    for (int i = 0; i < 100; i++) {
        bool actv = i % 3 == 0;
        struct a_struct new_str = { .is_active = actv, .a = 2*i };
        a_array[i] = new_str;
    }
    generic_func(a_array, a_str_is_active, sizeof(struct a_struct), 100);
}
